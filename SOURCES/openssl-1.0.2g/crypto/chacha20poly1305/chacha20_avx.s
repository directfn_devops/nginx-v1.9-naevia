
.text	
.align	16
chacha20_consts:
.byte	'e','x','p','a','n','d',' ','3','2','-','b','y','t','e',' ','k'
.rol8:
.byte	3,0,1,2, 7,4,5,6, 11,8,9,10, 15,12,13,14
.rol16:
.byte	2,3,0,1, 6,7,4,5, 10,11,8,9, 14,15,12,13
.avxInc:
.quad	1,0

.globl	chacha_20_core_avx
.type	chacha_20_core_avx ,@function
.align	64
chacha_20_core_avx:
	vzeroupper


	vmovdqa	.rol8(%rip),%xmm0
	vmovdqa	.rol16(%rip),%xmm1
	vmovdqu	32(%rcx),%xmm2

2:
	cmpq	$192,%rdx
	jb	2f

	vmovdqa	chacha20_consts(%rip),%xmm4
	vmovdqu	0(%rcx),%xmm5
	vmovdqu	16(%rcx),%xmm6
	vmovdqa	%xmm2,%xmm7

	vmovdqa	%xmm4,%xmm8
	vmovdqa	%xmm4,%xmm12

	vmovdqa	%xmm5,%xmm9
	vmovdqa	%xmm5,%xmm13

	vmovdqa	%xmm6,%xmm10
	vmovdqa	%xmm6,%xmm14

	vpaddq	.avxInc(%rip),%xmm7,%xmm11
	vpaddq	.avxInc(%rip),%xmm11,%xmm15

	movq	$10,%r8

1:

	vpaddd	%xmm5,%xmm4,%xmm4
	vpxor	%xmm4,%xmm7,%xmm7
	vpshufb	%xmm1,%xmm7,%xmm7

	vpaddd	%xmm7,%xmm6,%xmm6
	vpxor	%xmm6,%xmm5,%xmm5
	vpslld	$12,%xmm5,%xmm3
	vpsrld	$20,%xmm5,%xmm5
	vpxor	%xmm3,%xmm5,%xmm5

	vpaddd	%xmm5,%xmm4,%xmm4
	vpxor	%xmm4,%xmm7,%xmm7
	vpshufb	%xmm0,%xmm7,%xmm7

	vpaddd	%xmm7,%xmm6,%xmm6
	vpxor	%xmm6,%xmm5,%xmm5

	vpslld	$7,%xmm5,%xmm3
	vpsrld	$25,%xmm5,%xmm5
	vpxor	%xmm3,%xmm5,%xmm5

	vpaddd	%xmm9,%xmm8,%xmm8
	vpxor	%xmm8,%xmm11,%xmm11
	vpshufb	%xmm1,%xmm11,%xmm11

	vpaddd	%xmm11,%xmm10,%xmm10
	vpxor	%xmm10,%xmm9,%xmm9
	vpslld	$12,%xmm9,%xmm3
	vpsrld	$20,%xmm9,%xmm9
	vpxor	%xmm3,%xmm9,%xmm9

	vpaddd	%xmm9,%xmm8,%xmm8
	vpxor	%xmm8,%xmm11,%xmm11
	vpshufb	%xmm0,%xmm11,%xmm11

	vpaddd	%xmm11,%xmm10,%xmm10
	vpxor	%xmm10,%xmm9,%xmm9

	vpslld	$7,%xmm9,%xmm3
	vpsrld	$25,%xmm9,%xmm9
	vpxor	%xmm3,%xmm9,%xmm9

	vpaddd	%xmm13,%xmm12,%xmm12
	vpxor	%xmm12,%xmm15,%xmm15
	vpshufb	%xmm1,%xmm15,%xmm15

	vpaddd	%xmm15,%xmm14,%xmm14
	vpxor	%xmm14,%xmm13,%xmm13
	vpslld	$12,%xmm13,%xmm3
	vpsrld	$20,%xmm13,%xmm13
	vpxor	%xmm3,%xmm13,%xmm13

	vpaddd	%xmm13,%xmm12,%xmm12
	vpxor	%xmm12,%xmm15,%xmm15
	vpshufb	%xmm0,%xmm15,%xmm15

	vpaddd	%xmm15,%xmm14,%xmm14
	vpxor	%xmm14,%xmm13,%xmm13

	vpslld	$7,%xmm13,%xmm3
	vpsrld	$25,%xmm13,%xmm13
	vpxor	%xmm3,%xmm13,%xmm13
	vpalignr	$4,%xmm5,%xmm5,%xmm5
	vpalignr	$8,%xmm6,%xmm6,%xmm6
	vpalignr	$12,%xmm7,%xmm7,%xmm7
	vpalignr	$4,%xmm9,%xmm9,%xmm9
	vpalignr	$8,%xmm10,%xmm10,%xmm10
	vpalignr	$12,%xmm11,%xmm11,%xmm11
	vpalignr	$4,%xmm13,%xmm13,%xmm13
	vpalignr	$8,%xmm14,%xmm14,%xmm14
	vpalignr	$12,%xmm15,%xmm15,%xmm15

	vpaddd	%xmm5,%xmm4,%xmm4
	vpxor	%xmm4,%xmm7,%xmm7
	vpshufb	%xmm1,%xmm7,%xmm7

	vpaddd	%xmm7,%xmm6,%xmm6
	vpxor	%xmm6,%xmm5,%xmm5
	vpslld	$12,%xmm5,%xmm3
	vpsrld	$20,%xmm5,%xmm5
	vpxor	%xmm3,%xmm5,%xmm5

	vpaddd	%xmm5,%xmm4,%xmm4
	vpxor	%xmm4,%xmm7,%xmm7
	vpshufb	%xmm0,%xmm7,%xmm7

	vpaddd	%xmm7,%xmm6,%xmm6
	vpxor	%xmm6,%xmm5,%xmm5

	vpslld	$7,%xmm5,%xmm3
	vpsrld	$25,%xmm5,%xmm5
	vpxor	%xmm3,%xmm5,%xmm5

	vpaddd	%xmm9,%xmm8,%xmm8
	vpxor	%xmm8,%xmm11,%xmm11
	vpshufb	%xmm1,%xmm11,%xmm11

	vpaddd	%xmm11,%xmm10,%xmm10
	vpxor	%xmm10,%xmm9,%xmm9
	vpslld	$12,%xmm9,%xmm3
	vpsrld	$20,%xmm9,%xmm9
	vpxor	%xmm3,%xmm9,%xmm9

	vpaddd	%xmm9,%xmm8,%xmm8
	vpxor	%xmm8,%xmm11,%xmm11
	vpshufb	%xmm0,%xmm11,%xmm11

	vpaddd	%xmm11,%xmm10,%xmm10
	vpxor	%xmm10,%xmm9,%xmm9

	vpslld	$7,%xmm9,%xmm3
	vpsrld	$25,%xmm9,%xmm9
	vpxor	%xmm3,%xmm9,%xmm9

	vpaddd	%xmm13,%xmm12,%xmm12
	vpxor	%xmm12,%xmm15,%xmm15
	vpshufb	%xmm1,%xmm15,%xmm15

	vpaddd	%xmm15,%xmm14,%xmm14
	vpxor	%xmm14,%xmm13,%xmm13
	vpslld	$12,%xmm13,%xmm3
	vpsrld	$20,%xmm13,%xmm13
	vpxor	%xmm3,%xmm13,%xmm13

	vpaddd	%xmm13,%xmm12,%xmm12
	vpxor	%xmm12,%xmm15,%xmm15
	vpshufb	%xmm0,%xmm15,%xmm15

	vpaddd	%xmm15,%xmm14,%xmm14
	vpxor	%xmm14,%xmm13,%xmm13

	vpslld	$7,%xmm13,%xmm3
	vpsrld	$25,%xmm13,%xmm13
	vpxor	%xmm3,%xmm13,%xmm13
	vpalignr	$12,%xmm5,%xmm5,%xmm5
	vpalignr	$8,%xmm6,%xmm6,%xmm6
	vpalignr	$4,%xmm7,%xmm7,%xmm7
	vpalignr	$12,%xmm9,%xmm9,%xmm9
	vpalignr	$8,%xmm10,%xmm10,%xmm10
	vpalignr	$4,%xmm11,%xmm11,%xmm11
	vpalignr	$12,%xmm13,%xmm13,%xmm13
	vpalignr	$8,%xmm14,%xmm14,%xmm14
	vpalignr	$4,%xmm15,%xmm15,%xmm15

	decq	%r8

	jnz	1b

	vpaddd	chacha20_consts(%rip),%xmm4,%xmm4
	vpaddd	chacha20_consts(%rip),%xmm8,%xmm8
	vpaddd	chacha20_consts(%rip),%xmm12,%xmm12

	vpaddd	0(%rcx),%xmm5,%xmm5
	vpaddd	0(%rcx),%xmm9,%xmm9
	vpaddd	0(%rcx),%xmm13,%xmm13

	vpaddd	16(%rcx),%xmm6,%xmm6
	vpaddd	16(%rcx),%xmm10,%xmm10
	vpaddd	16(%rcx),%xmm14,%xmm14

	vpaddd	%xmm2,%xmm7,%xmm7
	vpaddq	.avxInc(%rip),%xmm2,%xmm2
	vpaddd	%xmm2,%xmm11,%xmm11
	vpaddq	.avxInc(%rip),%xmm2,%xmm2
	vpaddd	%xmm2,%xmm15,%xmm15
	vpaddq	.avxInc(%rip),%xmm2,%xmm2

	vpxor	0(%rsi),%xmm4,%xmm4
	vpxor	16(%rsi),%xmm5,%xmm5
	vpxor	32(%rsi),%xmm6,%xmm6
	vpxor	48(%rsi),%xmm7,%xmm7

	vmovdqu	%xmm4,0(%rdi)
	vmovdqu	%xmm5,16(%rdi)
	vmovdqu	%xmm6,32(%rdi)
	vmovdqu	%xmm7,48(%rdi)

	vpxor	64(%rsi),%xmm8,%xmm8
	vpxor	80(%rsi),%xmm9,%xmm9
	vpxor	96(%rsi),%xmm10,%xmm10
	vpxor	112(%rsi),%xmm11,%xmm11

	vmovdqu	%xmm8,64(%rdi)
	vmovdqu	%xmm9,80(%rdi)
	vmovdqu	%xmm10,96(%rdi)
	vmovdqu	%xmm11,112(%rdi)

	vpxor	128(%rsi),%xmm12,%xmm12
	vpxor	144(%rsi),%xmm13,%xmm13
	vpxor	160(%rsi),%xmm14,%xmm14
	vpxor	176(%rsi),%xmm15,%xmm15

	vmovdqu	%xmm12,128(%rdi)
	vmovdqu	%xmm13,144(%rdi)
	vmovdqu	%xmm14,160(%rdi)
	vmovdqu	%xmm15,176(%rdi)

	leaq	192(%rsi),%rsi
	leaq	192(%rdi),%rdi
	subq	$192,%rdx

	jmp	2b

2:
	cmpq	$128,%rdx
	jb	2f

	vmovdqa	chacha20_consts(%rip),%xmm4
	vmovdqa	chacha20_consts(%rip),%xmm8
	vmovdqu	0(%rcx),%xmm5
	vmovdqu	0(%rcx),%xmm9
	vmovdqu	16(%rcx),%xmm6
	vmovdqu	16(%rcx),%xmm10
	vmovdqu	16(%rcx),%xmm14
	vmovdqa	%xmm2,%xmm7
	vpaddq	.avxInc(%rip),%xmm7,%xmm11

	movq	$10,%r8

1:

	vpaddd	%xmm5,%xmm4,%xmm4
	vpxor	%xmm4,%xmm7,%xmm7
	vpshufb	%xmm1,%xmm7,%xmm7

	vpaddd	%xmm7,%xmm6,%xmm6
	vpxor	%xmm6,%xmm5,%xmm5
	vpslld	$12,%xmm5,%xmm3
	vpsrld	$20,%xmm5,%xmm5
	vpxor	%xmm3,%xmm5,%xmm5

	vpaddd	%xmm5,%xmm4,%xmm4
	vpxor	%xmm4,%xmm7,%xmm7
	vpshufb	%xmm0,%xmm7,%xmm7

	vpaddd	%xmm7,%xmm6,%xmm6
	vpxor	%xmm6,%xmm5,%xmm5

	vpslld	$7,%xmm5,%xmm3
	vpsrld	$25,%xmm5,%xmm5
	vpxor	%xmm3,%xmm5,%xmm5

	vpaddd	%xmm9,%xmm8,%xmm8
	vpxor	%xmm8,%xmm11,%xmm11
	vpshufb	%xmm1,%xmm11,%xmm11

	vpaddd	%xmm11,%xmm10,%xmm10
	vpxor	%xmm10,%xmm9,%xmm9
	vpslld	$12,%xmm9,%xmm3
	vpsrld	$20,%xmm9,%xmm9
	vpxor	%xmm3,%xmm9,%xmm9

	vpaddd	%xmm9,%xmm8,%xmm8
	vpxor	%xmm8,%xmm11,%xmm11
	vpshufb	%xmm0,%xmm11,%xmm11

	vpaddd	%xmm11,%xmm10,%xmm10
	vpxor	%xmm10,%xmm9,%xmm9

	vpslld	$7,%xmm9,%xmm3
	vpsrld	$25,%xmm9,%xmm9
	vpxor	%xmm3,%xmm9,%xmm9
	vpalignr	$4,%xmm5,%xmm5,%xmm5
	vpalignr	$8,%xmm6,%xmm6,%xmm6
	vpalignr	$12,%xmm7,%xmm7,%xmm7
	vpalignr	$4,%xmm9,%xmm9,%xmm9
	vpalignr	$8,%xmm10,%xmm10,%xmm10
	vpalignr	$12,%xmm11,%xmm11,%xmm11

	vpaddd	%xmm5,%xmm4,%xmm4
	vpxor	%xmm4,%xmm7,%xmm7
	vpshufb	%xmm1,%xmm7,%xmm7

	vpaddd	%xmm7,%xmm6,%xmm6
	vpxor	%xmm6,%xmm5,%xmm5
	vpslld	$12,%xmm5,%xmm3
	vpsrld	$20,%xmm5,%xmm5
	vpxor	%xmm3,%xmm5,%xmm5

	vpaddd	%xmm5,%xmm4,%xmm4
	vpxor	%xmm4,%xmm7,%xmm7
	vpshufb	%xmm0,%xmm7,%xmm7

	vpaddd	%xmm7,%xmm6,%xmm6
	vpxor	%xmm6,%xmm5,%xmm5

	vpslld	$7,%xmm5,%xmm3
	vpsrld	$25,%xmm5,%xmm5
	vpxor	%xmm3,%xmm5,%xmm5

	vpaddd	%xmm9,%xmm8,%xmm8
	vpxor	%xmm8,%xmm11,%xmm11
	vpshufb	%xmm1,%xmm11,%xmm11

	vpaddd	%xmm11,%xmm10,%xmm10
	vpxor	%xmm10,%xmm9,%xmm9
	vpslld	$12,%xmm9,%xmm3
	vpsrld	$20,%xmm9,%xmm9
	vpxor	%xmm3,%xmm9,%xmm9

	vpaddd	%xmm9,%xmm8,%xmm8
	vpxor	%xmm8,%xmm11,%xmm11
	vpshufb	%xmm0,%xmm11,%xmm11

	vpaddd	%xmm11,%xmm10,%xmm10
	vpxor	%xmm10,%xmm9,%xmm9

	vpslld	$7,%xmm9,%xmm3
	vpsrld	$25,%xmm9,%xmm9
	vpxor	%xmm3,%xmm9,%xmm9
	vpalignr	$12,%xmm5,%xmm5,%xmm5
	vpalignr	$8,%xmm6,%xmm6,%xmm6
	vpalignr	$4,%xmm7,%xmm7,%xmm7
	vpalignr	$12,%xmm9,%xmm9,%xmm9
	vpalignr	$8,%xmm10,%xmm10,%xmm10
	vpalignr	$4,%xmm11,%xmm11,%xmm11

	decq	%r8

	jnz	1b

	vpaddd	chacha20_consts(%rip),%xmm4,%xmm4
	vpaddd	chacha20_consts(%rip),%xmm8,%xmm8

	vpaddd	0(%rcx),%xmm5,%xmm5
	vpaddd	0(%rcx),%xmm9,%xmm9

	vpaddd	16(%rcx),%xmm6,%xmm6
	vpaddd	16(%rcx),%xmm10,%xmm10

	vpaddd	%xmm2,%xmm7,%xmm7
	vpaddq	.avxInc(%rip),%xmm2,%xmm2
	vpaddd	%xmm2,%xmm11,%xmm11
	vpaddq	.avxInc(%rip),%xmm2,%xmm2

	vpxor	0(%rsi),%xmm4,%xmm4
	vpxor	16(%rsi),%xmm5,%xmm5
	vpxor	32(%rsi),%xmm6,%xmm6
	vpxor	48(%rsi),%xmm7,%xmm7

	vmovdqu	%xmm4,0(%rdi)
	vmovdqu	%xmm5,16(%rdi)
	vmovdqu	%xmm6,32(%rdi)
	vmovdqu	%xmm7,48(%rdi)

	vpxor	64(%rsi),%xmm8,%xmm8
	vpxor	80(%rsi),%xmm9,%xmm9
	vpxor	96(%rsi),%xmm10,%xmm10
	vpxor	112(%rsi),%xmm11,%xmm11

	vmovdqu	%xmm8,64(%rdi)
	vmovdqu	%xmm9,80(%rdi)
	vmovdqu	%xmm10,96(%rdi)
	vmovdqu	%xmm11,112(%rdi)

	leaq	128(%rsi),%rsi
	leaq	128(%rdi),%rdi
	subq	$128,%rdx

	jmp	2b
2:
	cmpq	$64,%rdx
	jb	2f

	vmovdqa	chacha20_consts(%rip),%xmm4
	vmovdqu	0(%rcx),%xmm5
	vmovdqu	16(%rcx),%xmm6
	vmovdqa	%xmm2,%xmm7

	movq	$10,%r8

1:

	vpaddd	%xmm5,%xmm4,%xmm4
	vpxor	%xmm4,%xmm7,%xmm7
	vpshufb	%xmm1,%xmm7,%xmm7

	vpaddd	%xmm7,%xmm6,%xmm6
	vpxor	%xmm6,%xmm5,%xmm5
	vpslld	$12,%xmm5,%xmm3
	vpsrld	$20,%xmm5,%xmm5
	vpxor	%xmm3,%xmm5,%xmm5

	vpaddd	%xmm5,%xmm4,%xmm4
	vpxor	%xmm4,%xmm7,%xmm7
	vpshufb	%xmm0,%xmm7,%xmm7

	vpaddd	%xmm7,%xmm6,%xmm6
	vpxor	%xmm6,%xmm5,%xmm5

	vpslld	$7,%xmm5,%xmm3
	vpsrld	$25,%xmm5,%xmm5
	vpxor	%xmm3,%xmm5,%xmm5
	vpalignr	$4,%xmm5,%xmm5,%xmm5
	vpalignr	$8,%xmm6,%xmm6,%xmm6
	vpalignr	$12,%xmm7,%xmm7,%xmm7

	vpaddd	%xmm5,%xmm4,%xmm4
	vpxor	%xmm4,%xmm7,%xmm7
	vpshufb	%xmm1,%xmm7,%xmm7

	vpaddd	%xmm7,%xmm6,%xmm6
	vpxor	%xmm6,%xmm5,%xmm5
	vpslld	$12,%xmm5,%xmm3
	vpsrld	$20,%xmm5,%xmm5
	vpxor	%xmm3,%xmm5,%xmm5

	vpaddd	%xmm5,%xmm4,%xmm4
	vpxor	%xmm4,%xmm7,%xmm7
	vpshufb	%xmm0,%xmm7,%xmm7

	vpaddd	%xmm7,%xmm6,%xmm6
	vpxor	%xmm6,%xmm5,%xmm5

	vpslld	$7,%xmm5,%xmm3
	vpsrld	$25,%xmm5,%xmm5
	vpxor	%xmm3,%xmm5,%xmm5
	vpalignr	$12,%xmm5,%xmm5,%xmm5
	vpalignr	$8,%xmm6,%xmm6,%xmm6
	vpalignr	$4,%xmm7,%xmm7,%xmm7

	decq	%r8
	jnz	1b

	vpaddd	chacha20_consts(%rip),%xmm4,%xmm4
	vpaddd	0(%rcx),%xmm5,%xmm5
	vpaddd	16(%rcx),%xmm6,%xmm6
	vpaddd	%xmm2,%xmm7,%xmm7
	vpaddq	.avxInc(%rip),%xmm2,%xmm2

	vpxor	0(%rsi),%xmm4,%xmm4
	vpxor	16(%rsi),%xmm5,%xmm5
	vpxor	32(%rsi),%xmm6,%xmm6
	vpxor	48(%rsi),%xmm7,%xmm7

	vmovdqu	%xmm4,0(%rdi)
	vmovdqu	%xmm5,16(%rdi)
	vmovdqu	%xmm6,32(%rdi)
	vmovdqu	%xmm7,48(%rdi)

	leaq	64(%rsi),%rsi
	leaq	64(%rdi),%rdi
	subq	$64,%rdx
	jmp	2b

2:
	vmovdqu	%xmm2,32(%rcx)

	vzeroupper
	.byte	0xf3,0xc3
.size	chacha_20_core_avx,.-chacha_20_core_avx
