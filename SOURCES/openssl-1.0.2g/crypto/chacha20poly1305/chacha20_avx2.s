.text	
.align	32
chacha20_consts:
.byte	'e','x','p','a','n','d',' ','3','2','-','b','y','t','e',' ','k'
.byte	'e','x','p','a','n','d',' ','3','2','-','b','y','t','e',' ','k'
.rol8:
.byte	3,0,1,2, 7,4,5,6, 11,8,9,10, 15,12,13,14
.byte	3,0,1,2, 7,4,5,6, 11,8,9,10, 15,12,13,14
.rol16:
.byte	2,3,0,1, 6,7,4,5, 10,11,8,9, 14,15,12,13
.byte	2,3,0,1, 6,7,4,5, 10,11,8,9, 14,15,12,13
.avx2Init:
.quad	0,0,1,0
.avx2Inc:
.quad	2,0,2,0
.globl	chacha_20_core_avx2
.type	chacha_20_core_avx2 ,@function
.align	64
chacha_20_core_avx2:

	vzeroupper


	vbroadcasti128	0(%rcx),%ymm0
	vbroadcasti128	16(%rcx),%ymm1
	vbroadcasti128	32(%rcx),%ymm2
	vpaddq	.avx2Init(%rip),%ymm2,%ymm2

2:
	cmpq	$384,%rdx
	jb	2f

	vmovdqa	chacha20_consts(%rip),%ymm4
	vmovdqa	chacha20_consts(%rip),%ymm8
	vmovdqa	chacha20_consts(%rip),%ymm12

	vmovdqa	%ymm0,%ymm5
	vmovdqa	%ymm0,%ymm9
	vmovdqa	%ymm0,%ymm13

	vmovdqa	%ymm1,%ymm6
	vmovdqa	%ymm1,%ymm10
	vmovdqa	%ymm1,%ymm14

	vmovdqa	%ymm2,%ymm7
	vpaddq	.avx2Inc(%rip),%ymm7,%ymm11
	vpaddq	.avx2Inc(%rip),%ymm11,%ymm15

	movq	$10,%r8

1:

	vpaddd	%ymm5,%ymm4,%ymm4
	vpxor	%ymm4,%ymm7,%ymm7
	vpshufb	.rol16(%rip),%ymm7,%ymm7

	vpaddd	%ymm7,%ymm6,%ymm6
	vpxor	%ymm6,%ymm5,%ymm5
	vpslld	$12,%ymm5,%ymm3
	vpsrld	$20,%ymm5,%ymm5
	vpxor	%ymm3,%ymm5,%ymm5

	vpaddd	%ymm5,%ymm4,%ymm4
	vpxor	%ymm4,%ymm7,%ymm7
	vpshufb	.rol8(%rip),%ymm7,%ymm7

	vpaddd	%ymm7,%ymm6,%ymm6
	vpxor	%ymm6,%ymm5,%ymm5

	vpslld	$7,%ymm5,%ymm3
	vpsrld	$25,%ymm5,%ymm5
	vpxor	%ymm3,%ymm5,%ymm5

	vpaddd	%ymm9,%ymm8,%ymm8
	vpxor	%ymm8,%ymm11,%ymm11
	vpshufb	.rol16(%rip),%ymm11,%ymm11

	vpaddd	%ymm11,%ymm10,%ymm10
	vpxor	%ymm10,%ymm9,%ymm9
	vpslld	$12,%ymm9,%ymm3
	vpsrld	$20,%ymm9,%ymm9
	vpxor	%ymm3,%ymm9,%ymm9

	vpaddd	%ymm9,%ymm8,%ymm8
	vpxor	%ymm8,%ymm11,%ymm11
	vpshufb	.rol8(%rip),%ymm11,%ymm11

	vpaddd	%ymm11,%ymm10,%ymm10
	vpxor	%ymm10,%ymm9,%ymm9

	vpslld	$7,%ymm9,%ymm3
	vpsrld	$25,%ymm9,%ymm9
	vpxor	%ymm3,%ymm9,%ymm9

	vpaddd	%ymm13,%ymm12,%ymm12
	vpxor	%ymm12,%ymm15,%ymm15
	vpshufb	.rol16(%rip),%ymm15,%ymm15

	vpaddd	%ymm15,%ymm14,%ymm14
	vpxor	%ymm14,%ymm13,%ymm13
	vpslld	$12,%ymm13,%ymm3
	vpsrld	$20,%ymm13,%ymm13
	vpxor	%ymm3,%ymm13,%ymm13

	vpaddd	%ymm13,%ymm12,%ymm12
	vpxor	%ymm12,%ymm15,%ymm15
	vpshufb	.rol8(%rip),%ymm15,%ymm15

	vpaddd	%ymm15,%ymm14,%ymm14
	vpxor	%ymm14,%ymm13,%ymm13

	vpslld	$7,%ymm13,%ymm3
	vpsrld	$25,%ymm13,%ymm13
	vpxor	%ymm3,%ymm13,%ymm13
	vpalignr	$4,%ymm5,%ymm5,%ymm5
	vpalignr	$8,%ymm6,%ymm6,%ymm6
	vpalignr	$12,%ymm7,%ymm7,%ymm7
	vpalignr	$4,%ymm9,%ymm9,%ymm9
	vpalignr	$8,%ymm10,%ymm10,%ymm10
	vpalignr	$12,%ymm11,%ymm11,%ymm11
	vpalignr	$4,%ymm13,%ymm13,%ymm13
	vpalignr	$8,%ymm14,%ymm14,%ymm14
	vpalignr	$12,%ymm15,%ymm15,%ymm15

	vpaddd	%ymm5,%ymm4,%ymm4
	vpxor	%ymm4,%ymm7,%ymm7
	vpshufb	.rol16(%rip),%ymm7,%ymm7

	vpaddd	%ymm7,%ymm6,%ymm6
	vpxor	%ymm6,%ymm5,%ymm5
	vpslld	$12,%ymm5,%ymm3
	vpsrld	$20,%ymm5,%ymm5
	vpxor	%ymm3,%ymm5,%ymm5

	vpaddd	%ymm5,%ymm4,%ymm4
	vpxor	%ymm4,%ymm7,%ymm7
	vpshufb	.rol8(%rip),%ymm7,%ymm7

	vpaddd	%ymm7,%ymm6,%ymm6
	vpxor	%ymm6,%ymm5,%ymm5

	vpslld	$7,%ymm5,%ymm3
	vpsrld	$25,%ymm5,%ymm5
	vpxor	%ymm3,%ymm5,%ymm5

	vpaddd	%ymm9,%ymm8,%ymm8
	vpxor	%ymm8,%ymm11,%ymm11
	vpshufb	.rol16(%rip),%ymm11,%ymm11

	vpaddd	%ymm11,%ymm10,%ymm10
	vpxor	%ymm10,%ymm9,%ymm9
	vpslld	$12,%ymm9,%ymm3
	vpsrld	$20,%ymm9,%ymm9
	vpxor	%ymm3,%ymm9,%ymm9

	vpaddd	%ymm9,%ymm8,%ymm8
	vpxor	%ymm8,%ymm11,%ymm11
	vpshufb	.rol8(%rip),%ymm11,%ymm11

	vpaddd	%ymm11,%ymm10,%ymm10
	vpxor	%ymm10,%ymm9,%ymm9

	vpslld	$7,%ymm9,%ymm3
	vpsrld	$25,%ymm9,%ymm9
	vpxor	%ymm3,%ymm9,%ymm9

	vpaddd	%ymm13,%ymm12,%ymm12
	vpxor	%ymm12,%ymm15,%ymm15
	vpshufb	.rol16(%rip),%ymm15,%ymm15

	vpaddd	%ymm15,%ymm14,%ymm14
	vpxor	%ymm14,%ymm13,%ymm13
	vpslld	$12,%ymm13,%ymm3
	vpsrld	$20,%ymm13,%ymm13
	vpxor	%ymm3,%ymm13,%ymm13

	vpaddd	%ymm13,%ymm12,%ymm12
	vpxor	%ymm12,%ymm15,%ymm15
	vpshufb	.rol8(%rip),%ymm15,%ymm15

	vpaddd	%ymm15,%ymm14,%ymm14
	vpxor	%ymm14,%ymm13,%ymm13

	vpslld	$7,%ymm13,%ymm3
	vpsrld	$25,%ymm13,%ymm13
	vpxor	%ymm3,%ymm13,%ymm13
	vpalignr	$12,%ymm5,%ymm5,%ymm5
	vpalignr	$8,%ymm6,%ymm6,%ymm6
	vpalignr	$4,%ymm7,%ymm7,%ymm7
	vpalignr	$12,%ymm9,%ymm9,%ymm9
	vpalignr	$8,%ymm10,%ymm10,%ymm10
	vpalignr	$4,%ymm11,%ymm11,%ymm11
	vpalignr	$12,%ymm13,%ymm13,%ymm13
	vpalignr	$8,%ymm14,%ymm14,%ymm14
	vpalignr	$4,%ymm15,%ymm15,%ymm15

	decq	%r8

	jnz	1b

	vpaddd	chacha20_consts(%rip),%ymm4,%ymm4
	vpaddd	chacha20_consts(%rip),%ymm8,%ymm8
	vpaddd	chacha20_consts(%rip),%ymm12,%ymm12

	vpaddd	%ymm0,%ymm5,%ymm5
	vpaddd	%ymm0,%ymm9,%ymm9
	vpaddd	%ymm0,%ymm13,%ymm13

	vpaddd	%ymm1,%ymm6,%ymm6
	vpaddd	%ymm1,%ymm10,%ymm10
	vpaddd	%ymm1,%ymm14,%ymm14

	vpaddd	%ymm2,%ymm7,%ymm7
	vpaddq	.avx2Inc(%rip),%ymm2,%ymm2
	vpaddd	%ymm2,%ymm11,%ymm11
	vpaddq	.avx2Inc(%rip),%ymm2,%ymm2
	vpaddd	%ymm2,%ymm15,%ymm15
	vpaddq	.avx2Inc(%rip),%ymm2,%ymm2

	vperm2i128	$0x02,%ymm4,%ymm5,%ymm3
	vpxor	0(%rsi),%ymm3,%ymm3
	vmovdqu	%ymm3,0(%rdi)
	vperm2i128	$0x02,%ymm6,%ymm7,%ymm3
	vpxor	32(%rsi),%ymm3,%ymm3
	vmovdqu	%ymm3,32(%rdi)
	vperm2i128	$0x13,%ymm4,%ymm5,%ymm3
	vpxor	64(%rsi),%ymm3,%ymm3
	vmovdqu	%ymm3,64(%rdi)
	vperm2i128	$0x13,%ymm6,%ymm7,%ymm3
	vpxor	96(%rsi),%ymm3,%ymm3
	vmovdqu	%ymm3,96(%rdi)

	vperm2i128	$0x02,%ymm8,%ymm9,%ymm4
	vperm2i128	$0x02,%ymm10,%ymm11,%ymm5
	vperm2i128	$0x13,%ymm8,%ymm9,%ymm6
	vperm2i128	$0x13,%ymm10,%ymm11,%ymm7

	vpxor	128(%rsi),%ymm4,%ymm4
	vpxor	160(%rsi),%ymm5,%ymm5
	vpxor	192(%rsi),%ymm6,%ymm6
	vpxor	224(%rsi),%ymm7,%ymm7

	vmovdqu	%ymm4,128(%rdi)
	vmovdqu	%ymm5,160(%rdi)
	vmovdqu	%ymm6,192(%rdi)
	vmovdqu	%ymm7,224(%rdi)

	vperm2i128	$0x02,%ymm12,%ymm13,%ymm4
	vperm2i128	$0x02,%ymm14,%ymm15,%ymm5
	vperm2i128	$0x13,%ymm12,%ymm13,%ymm6
	vperm2i128	$0x13,%ymm14,%ymm15,%ymm7

	vpxor	256(%rsi),%ymm4,%ymm4
	vpxor	288(%rsi),%ymm5,%ymm5
	vpxor	320(%rsi),%ymm6,%ymm6
	vpxor	352(%rsi),%ymm7,%ymm7

	vmovdqu	%ymm4,256(%rdi)
	vmovdqu	%ymm5,288(%rdi)
	vmovdqu	%ymm6,320(%rdi)
	vmovdqu	%ymm7,352(%rdi)

	leaq	384(%rsi),%rsi
	leaq	384(%rdi),%rdi
	subq	$384,%rdx

	jmp	2b

2:
	cmpq	$256,%rdx
	jb	2f

	vmovdqa	chacha20_consts(%rip),%ymm4
	vmovdqa	chacha20_consts(%rip),%ymm8
	vmovdqa	%ymm0,%ymm5
	vmovdqa	%ymm0,%ymm9
	vmovdqa	%ymm1,%ymm6
	vmovdqa	%ymm1,%ymm10
	vmovdqa	%ymm1,%ymm14
	vmovdqa	%ymm2,%ymm7
	vpaddq	.avx2Inc(%rip),%ymm7,%ymm11

	movq	$10,%r8

1:

	vpaddd	%ymm5,%ymm4,%ymm4
	vpxor	%ymm4,%ymm7,%ymm7
	vpshufb	.rol16(%rip),%ymm7,%ymm7

	vpaddd	%ymm7,%ymm6,%ymm6
	vpxor	%ymm6,%ymm5,%ymm5
	vpslld	$12,%ymm5,%ymm3
	vpsrld	$20,%ymm5,%ymm5
	vpxor	%ymm3,%ymm5,%ymm5

	vpaddd	%ymm5,%ymm4,%ymm4
	vpxor	%ymm4,%ymm7,%ymm7
	vpshufb	.rol8(%rip),%ymm7,%ymm7

	vpaddd	%ymm7,%ymm6,%ymm6
	vpxor	%ymm6,%ymm5,%ymm5

	vpslld	$7,%ymm5,%ymm3
	vpsrld	$25,%ymm5,%ymm5
	vpxor	%ymm3,%ymm5,%ymm5

	vpaddd	%ymm9,%ymm8,%ymm8
	vpxor	%ymm8,%ymm11,%ymm11
	vpshufb	.rol16(%rip),%ymm11,%ymm11

	vpaddd	%ymm11,%ymm10,%ymm10
	vpxor	%ymm10,%ymm9,%ymm9
	vpslld	$12,%ymm9,%ymm3
	vpsrld	$20,%ymm9,%ymm9
	vpxor	%ymm3,%ymm9,%ymm9

	vpaddd	%ymm9,%ymm8,%ymm8
	vpxor	%ymm8,%ymm11,%ymm11
	vpshufb	.rol8(%rip),%ymm11,%ymm11

	vpaddd	%ymm11,%ymm10,%ymm10
	vpxor	%ymm10,%ymm9,%ymm9

	vpslld	$7,%ymm9,%ymm3
	vpsrld	$25,%ymm9,%ymm9
	vpxor	%ymm3,%ymm9,%ymm9
	vpalignr	$4,%ymm5,%ymm5,%ymm5
	vpalignr	$8,%ymm6,%ymm6,%ymm6
	vpalignr	$12,%ymm7,%ymm7,%ymm7
	vpalignr	$4,%ymm9,%ymm9,%ymm9
	vpalignr	$8,%ymm10,%ymm10,%ymm10
	vpalignr	$12,%ymm11,%ymm11,%ymm11

	vpaddd	%ymm5,%ymm4,%ymm4
	vpxor	%ymm4,%ymm7,%ymm7
	vpshufb	.rol16(%rip),%ymm7,%ymm7

	vpaddd	%ymm7,%ymm6,%ymm6
	vpxor	%ymm6,%ymm5,%ymm5
	vpslld	$12,%ymm5,%ymm3
	vpsrld	$20,%ymm5,%ymm5
	vpxor	%ymm3,%ymm5,%ymm5

	vpaddd	%ymm5,%ymm4,%ymm4
	vpxor	%ymm4,%ymm7,%ymm7
	vpshufb	.rol8(%rip),%ymm7,%ymm7

	vpaddd	%ymm7,%ymm6,%ymm6
	vpxor	%ymm6,%ymm5,%ymm5

	vpslld	$7,%ymm5,%ymm3
	vpsrld	$25,%ymm5,%ymm5
	vpxor	%ymm3,%ymm5,%ymm5

	vpaddd	%ymm9,%ymm8,%ymm8
	vpxor	%ymm8,%ymm11,%ymm11
	vpshufb	.rol16(%rip),%ymm11,%ymm11

	vpaddd	%ymm11,%ymm10,%ymm10
	vpxor	%ymm10,%ymm9,%ymm9
	vpslld	$12,%ymm9,%ymm3
	vpsrld	$20,%ymm9,%ymm9
	vpxor	%ymm3,%ymm9,%ymm9

	vpaddd	%ymm9,%ymm8,%ymm8
	vpxor	%ymm8,%ymm11,%ymm11
	vpshufb	.rol8(%rip),%ymm11,%ymm11

	vpaddd	%ymm11,%ymm10,%ymm10
	vpxor	%ymm10,%ymm9,%ymm9

	vpslld	$7,%ymm9,%ymm3
	vpsrld	$25,%ymm9,%ymm9
	vpxor	%ymm3,%ymm9,%ymm9
	vpalignr	$12,%ymm5,%ymm5,%ymm5
	vpalignr	$8,%ymm6,%ymm6,%ymm6
	vpalignr	$4,%ymm7,%ymm7,%ymm7
	vpalignr	$12,%ymm9,%ymm9,%ymm9
	vpalignr	$8,%ymm10,%ymm10,%ymm10
	vpalignr	$4,%ymm11,%ymm11,%ymm11

	decq	%r8

	jnz	1b

	vpaddd	chacha20_consts(%rip),%ymm4,%ymm4
	vpaddd	chacha20_consts(%rip),%ymm8,%ymm8

	vpaddd	%ymm0,%ymm5,%ymm5
	vpaddd	%ymm0,%ymm9,%ymm9

	vpaddd	%ymm1,%ymm6,%ymm6
	vpaddd	%ymm1,%ymm10,%ymm10

	vpaddd	%ymm2,%ymm7,%ymm7
	vpaddq	.avx2Inc(%rip),%ymm2,%ymm2
	vpaddd	%ymm2,%ymm11,%ymm11
	vpaddq	.avx2Inc(%rip),%ymm2,%ymm2

	vperm2i128	$0x02,%ymm4,%ymm5,%ymm12
	vperm2i128	$0x02,%ymm6,%ymm7,%ymm13
	vperm2i128	$0x13,%ymm4,%ymm5,%ymm14
	vperm2i128	$0x13,%ymm6,%ymm7,%ymm15

	vpxor	0(%rsi),%ymm12,%ymm12
	vpxor	32(%rsi),%ymm13,%ymm13
	vpxor	64(%rsi),%ymm14,%ymm14
	vpxor	96(%rsi),%ymm15,%ymm15

	vmovdqu	%ymm12,0(%rdi)
	vmovdqu	%ymm13,32(%rdi)
	vmovdqu	%ymm14,64(%rdi)
	vmovdqu	%ymm15,96(%rdi)

	vperm2i128	$0x02,%ymm8,%ymm9,%ymm4
	vperm2i128	$0x02,%ymm10,%ymm11,%ymm5
	vperm2i128	$0x13,%ymm8,%ymm9,%ymm6
	vperm2i128	$0x13,%ymm10,%ymm11,%ymm7

	vpxor	128(%rsi),%ymm4,%ymm4
	vpxor	160(%rsi),%ymm5,%ymm5
	vpxor	192(%rsi),%ymm6,%ymm6
	vpxor	224(%rsi),%ymm7,%ymm7

	vmovdqu	%ymm4,128(%rdi)
	vmovdqu	%ymm5,160(%rdi)
	vmovdqu	%ymm6,192(%rdi)
	vmovdqu	%ymm7,224(%rdi)

	leaq	256(%rsi),%rsi
	leaq	256(%rdi),%rdi
	subq	$256,%rdx

	jmp	2b
2:
	cmpq	$128,%rdx
	jb	2f

	vmovdqa	chacha20_consts(%rip),%ymm4
	vmovdqa	%ymm0,%ymm5
	vmovdqa	%ymm1,%ymm6
	vmovdqa	%ymm2,%ymm7

	movq	$10,%r8

1:

	vpaddd	%ymm5,%ymm4,%ymm4
	vpxor	%ymm4,%ymm7,%ymm7
	vpshufb	.rol16(%rip),%ymm7,%ymm7

	vpaddd	%ymm7,%ymm6,%ymm6
	vpxor	%ymm6,%ymm5,%ymm5
	vpslld	$12,%ymm5,%ymm3
	vpsrld	$20,%ymm5,%ymm5
	vpxor	%ymm3,%ymm5,%ymm5

	vpaddd	%ymm5,%ymm4,%ymm4
	vpxor	%ymm4,%ymm7,%ymm7
	vpshufb	.rol8(%rip),%ymm7,%ymm7

	vpaddd	%ymm7,%ymm6,%ymm6
	vpxor	%ymm6,%ymm5,%ymm5

	vpslld	$7,%ymm5,%ymm3
	vpsrld	$25,%ymm5,%ymm5
	vpxor	%ymm3,%ymm5,%ymm5
	vpalignr	$4,%ymm5,%ymm5,%ymm5
	vpalignr	$8,%ymm6,%ymm6,%ymm6
	vpalignr	$12,%ymm7,%ymm7,%ymm7

	vpaddd	%ymm5,%ymm4,%ymm4
	vpxor	%ymm4,%ymm7,%ymm7
	vpshufb	.rol16(%rip),%ymm7,%ymm7

	vpaddd	%ymm7,%ymm6,%ymm6
	vpxor	%ymm6,%ymm5,%ymm5
	vpslld	$12,%ymm5,%ymm3
	vpsrld	$20,%ymm5,%ymm5
	vpxor	%ymm3,%ymm5,%ymm5

	vpaddd	%ymm5,%ymm4,%ymm4
	vpxor	%ymm4,%ymm7,%ymm7
	vpshufb	.rol8(%rip),%ymm7,%ymm7

	vpaddd	%ymm7,%ymm6,%ymm6
	vpxor	%ymm6,%ymm5,%ymm5

	vpslld	$7,%ymm5,%ymm3
	vpsrld	$25,%ymm5,%ymm5
	vpxor	%ymm3,%ymm5,%ymm5
	vpalignr	$12,%ymm5,%ymm5,%ymm5
	vpalignr	$8,%ymm6,%ymm6,%ymm6
	vpalignr	$4,%ymm7,%ymm7,%ymm7

	decq	%r8
	jnz	1b

	vpaddd	chacha20_consts(%rip),%ymm4,%ymm4
	vpaddd	%ymm0,%ymm5,%ymm5
	vpaddd	%ymm1,%ymm6,%ymm6
	vpaddd	%ymm2,%ymm7,%ymm7
	vpaddq	.avx2Inc(%rip),%ymm2,%ymm2

	vperm2i128	$0x02,%ymm4,%ymm5,%ymm12
	vperm2i128	$0x02,%ymm6,%ymm7,%ymm13
	vperm2i128	$0x13,%ymm4,%ymm5,%ymm14
	vperm2i128	$0x13,%ymm6,%ymm7,%ymm15

	vpxor	0(%rsi),%ymm12,%ymm12
	vpxor	32(%rsi),%ymm13,%ymm13
	vpxor	64(%rsi),%ymm14,%ymm14
	vpxor	96(%rsi),%ymm15,%ymm15

	vmovdqu	%ymm12,0(%rdi)
	vmovdqu	%ymm13,32(%rdi)
	vmovdqu	%ymm14,64(%rdi)
	vmovdqu	%ymm15,96(%rdi)

	leaq	128(%rsi),%rsi
	leaq	128(%rdi),%rdi
	subq	$128,%rdx
	jmp	2b

2:
	vmovdqu	%xmm2,32(%rcx)

	vzeroupper
	.byte	0xf3,0xc3
.size	chacha_20_core_avx2,.-chacha_20_core_avx2
