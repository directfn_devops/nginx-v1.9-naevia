.text	
.align	32
.LandMask:
.quad	0x3FFFFFF, 0x3FFFFFF, 0x3FFFFFF, 0x3FFFFFF
.LsetBit:
.quad	0x1000000, 0x1000000, 0x1000000, 0x1000000
.LrSet:
.quad	0xFFFFFFC0FFFFFFF, 0xFFFFFFC0FFFFFFF, 0xFFFFFFC0FFFFFFF, 0xFFFFFFC0FFFFFFF
.quad	0xFFFFFFC0FFFFFFC, 0xFFFFFFC0FFFFFFC, 0xFFFFFFC0FFFFFFC, 0xFFFFFFC0FFFFFFC

.LpermFix:
.long	6,7,6,7,6,7,6,7
.long	4,5,6,7,6,7,6,7
.long	2,3,6,7,4,5,6,7
.long	0,1,4,5,2,3,6,7



.type	poly1305_init_avx2, @function
.align	64
poly1305_init_avx2:
	vzeroupper


	movq	0(%rdi),%r8
	movq	8(%rdi),%r9
	movq	16(%rdi),%r10

	movq	%r8,%rax
	movq	%r9,%rcx

	subq	$-5,%r8
	sbbq	$-1,%r9
	sbbq	$3,%r10

	cmovcq	%rax,%r8
	cmovcq	%rcx,%r9
	cmovcq	16(%rdi),%r10

	movq	%r8,%rax
	andq	$0x3ffffff,%rax
	movl	%eax,352(%rdi)

	shrdq	$26,%r9,%r8
	shrdq	$26,%r10,%r9

	movq	%r8,%rax
	andq	$0x3ffffff,%rax
	movl	%eax,356(%rdi)

	shrdq	$26,%r9,%r8
	shrq	$26,%r9

	movq	%r8,%rax
	andq	$0x3ffffff,%rax
	movl	%eax,360(%rdi)

	shrdq	$26,%r9,%r8

	movq	%r8,%rax
	andq	$0x3ffffff,%rax
	movl	%eax,364(%rdi)

	shrq	$26,%r8

	movq	%r8,%rax
	andq	$0x3ffffff,%rax
	movl	%eax,368(%rdi)


	vmovq	24(%rdi),%xmm5
	vmovq	32(%rdi),%xmm10
	vpand	.LrSet(%rip),%xmm5,%xmm5
	vpand	.LrSet+32(%rip),%xmm10,%xmm10

	vpsrlq	$26,%xmm5,%xmm6
	vpand	.LandMask(%rip),%xmm5,%xmm5
	vpsrlq	$26,%xmm6,%xmm7
	vpand	.LandMask(%rip),%xmm6,%xmm6
	vpsllq	$12,%xmm10,%xmm11
	vpxor	%xmm11,%xmm7,%xmm7
	vpsrlq	$26,%xmm7,%xmm8
	vpsrlq	$40,%xmm10,%xmm9
	vpand	.LandMask(%rip),%xmm7,%xmm7
	vpand	.LandMask(%rip),%xmm8,%xmm8

	vpmuludq	%xmm5,%xmm5,%xmm0
	vpmuludq	%xmm6,%xmm5,%xmm1
	vpmuludq	%xmm7,%xmm5,%xmm2
	vpmuludq	%xmm8,%xmm5,%xmm3
	vpmuludq	%xmm9,%xmm5,%xmm4

	vpsllq	$1,%xmm1,%xmm1
	vpsllq	$1,%xmm2,%xmm2
	vpmuludq	%xmm6,%xmm6,%xmm10
	vpaddq	%xmm10,%xmm2,%xmm2
	vpmuludq	%xmm7,%xmm6,%xmm10
	vpaddq	%xmm10,%xmm3,%xmm3
	vpmuludq	%xmm8,%xmm6,%xmm10
	vpaddq	%xmm10,%xmm4,%xmm4
	vpmuludq	%xmm9,%xmm6,%xmm12

	vpsllq	$1,%xmm3,%xmm3
	vpsllq	$1,%xmm4,%xmm4
	vpmuludq	%xmm7,%xmm7,%xmm10
	vpaddq	%xmm10,%xmm4,%xmm4
	vpmuludq	%xmm8,%xmm7,%xmm10
	vpaddq	%xmm10,%xmm12,%xmm12
	vpmuludq	%xmm9,%xmm7,%xmm13

	vpsllq	$1,%xmm12,%xmm12
	vpsllq	$1,%xmm13,%xmm13
	vpmuludq	%xmm8,%xmm8,%xmm10
	vpaddq	%xmm10,%xmm13,%xmm13
	vpmuludq	%xmm9,%xmm8,%xmm14

	vpsllq	$1,%xmm14,%xmm14
	vpmuludq	%xmm9,%xmm9,%xmm15


	vpsrlq	$26,%xmm4,%xmm10
	vpand	.LandMask(%rip),%xmm4,%xmm4
	vpaddq	%xmm10,%xmm12,%xmm12

	vpsllq	$2,%xmm12,%xmm10
	vpaddq	%xmm10,%xmm12,%xmm12
	vpsllq	$2,%xmm13,%xmm10
	vpaddq	%xmm10,%xmm13,%xmm13
	vpsllq	$2,%xmm14,%xmm10
	vpaddq	%xmm10,%xmm14,%xmm14
	vpsllq	$2,%xmm15,%xmm10
	vpaddq	%xmm10,%xmm15,%xmm15

	vpaddq	%xmm12,%xmm0,%xmm0
	vpaddq	%xmm13,%xmm1,%xmm1
	vpaddq	%xmm14,%xmm2,%xmm2
	vpaddq	%xmm15,%xmm3,%xmm3

	vpsrlq	$26,%xmm0,%xmm10
	vpand	.LandMask(%rip),%xmm0,%xmm0
	vpaddq	%xmm10,%xmm1,%xmm1
	vpsrlq	$26,%xmm1,%xmm10
	vpand	.LandMask(%rip),%xmm1,%xmm1
	vpaddq	%xmm10,%xmm2,%xmm2
	vpsrlq	$26,%xmm2,%xmm10
	vpand	.LandMask(%rip),%xmm2,%xmm2
	vpaddq	%xmm10,%xmm3,%xmm3
	vpsrlq	$26,%xmm3,%xmm10
	vpand	.LandMask(%rip),%xmm3,%xmm3
	vpaddq	%xmm10,%xmm4,%xmm4

	vpunpcklqdq	%xmm5,%xmm0,%xmm5
	vpunpcklqdq	%xmm6,%xmm1,%xmm6
	vpunpcklqdq	%xmm7,%xmm2,%xmm7
	vpunpcklqdq	%xmm8,%xmm3,%xmm8
	vpunpcklqdq	%xmm9,%xmm4,%xmm9

	vmovdqu	%xmm5,64+16(%rdi)
	vmovdqu	%xmm6,96+16(%rdi)
	vmovdqu	%xmm7,128+16(%rdi)
	vmovdqu	%xmm8,160+16(%rdi)
	vmovdqu	%xmm9,192+16(%rdi)

	vpsllq	$2,%xmm6,%xmm1
	vpsllq	$2,%xmm7,%xmm2
	vpsllq	$2,%xmm8,%xmm3
	vpsllq	$2,%xmm9,%xmm4

	vpaddq	%xmm1,%xmm6,%xmm1
	vpaddq	%xmm2,%xmm7,%xmm2
	vpaddq	%xmm3,%xmm8,%xmm3
	vpaddq	%xmm4,%xmm9,%xmm4

	vmovdqu	%xmm1,224+16(%rdi)
	vmovdqu	%xmm2,256+16(%rdi)
	vmovdqu	%xmm3,288+16(%rdi)
	vmovdqu	%xmm4,320+16(%rdi)


	vpshufd	$0x44,%xmm5,%xmm0
	vpshufd	$0x44,%xmm6,%xmm1
	vpshufd	$0x44,%xmm7,%xmm2
	vpshufd	$0x44,%xmm8,%xmm3
	vpshufd	$0x44,%xmm9,%xmm4


	vmovdqu	64+16(%rdi),%xmm10
	vpmuludq	%xmm10,%xmm0,%xmm5
	vpmuludq	%xmm10,%xmm1,%xmm6
	vpmuludq	%xmm10,%xmm2,%xmm7
	vpmuludq	%xmm10,%xmm3,%xmm8
	vpmuludq	%xmm10,%xmm4,%xmm9

	vmovdqu	224+16(%rdi),%xmm10
	vpmuludq	%xmm10,%xmm4,%xmm11
	vpaddq	%xmm11,%xmm5,%xmm5
	vmovdqu	96+16(%rdi),%xmm10
	vpmuludq	%xmm10,%xmm0,%xmm11
	vpaddq	%xmm11,%xmm6,%xmm6
	vpmuludq	%xmm10,%xmm1,%xmm11
	vpaddq	%xmm11,%xmm7,%xmm7
	vpmuludq	%xmm10,%xmm2,%xmm11
	vpaddq	%xmm11,%xmm8,%xmm8
	vpmuludq	%xmm10,%xmm3,%xmm11
	vpaddq	%xmm11,%xmm9,%xmm9

	vmovdqu	256+16(%rdi),%xmm10
	vpmuludq	%xmm10,%xmm3,%xmm11
	vpaddq	%xmm11,%xmm5,%xmm5
	vpmuludq	%xmm10,%xmm4,%xmm11
	vpaddq	%xmm11,%xmm6,%xmm6
	vmovdqu	128+16(%rdi),%xmm10
	vpmuludq	%xmm10,%xmm0,%xmm11
	vpaddq	%xmm11,%xmm7,%xmm7
	vpmuludq	%xmm10,%xmm1,%xmm11
	vpaddq	%xmm11,%xmm8,%xmm8
	vpmuludq	%xmm10,%xmm2,%xmm11
	vpaddq	%xmm11,%xmm9,%xmm9

	vmovdqu	288+16(%rdi),%xmm10
	vpmuludq	%xmm10,%xmm2,%xmm11
	vpaddq	%xmm11,%xmm5,%xmm5
	vpmuludq	%xmm10,%xmm3,%xmm11
	vpaddq	%xmm11,%xmm6,%xmm6
	vpmuludq	%xmm10,%xmm4,%xmm11
	vpaddq	%xmm11,%xmm7,%xmm7
	vmovdqu	160+16(%rdi),%xmm10
	vpmuludq	%xmm10,%xmm0,%xmm11
	vpaddq	%xmm11,%xmm8,%xmm8
	vpmuludq	%xmm10,%xmm1,%xmm11
	vpaddq	%xmm11,%xmm9,%xmm9

	vmovdqu	320+16(%rdi),%xmm10
	vpmuludq	%xmm10,%xmm1,%xmm11
	vpaddq	%xmm11,%xmm5,%xmm5
	vpmuludq	%xmm10,%xmm2,%xmm11
	vpaddq	%xmm11,%xmm6,%xmm6
	vpmuludq	%xmm10,%xmm3,%xmm11
	vpaddq	%xmm11,%xmm7,%xmm7
	vpmuludq	%xmm10,%xmm4,%xmm11
	vpaddq	%xmm11,%xmm8,%xmm8
	vmovdqu	192+16(%rdi),%xmm10
	vpmuludq	%xmm10,%xmm0,%xmm11
	vpaddq	%xmm11,%xmm9,%xmm9

	vpsrlq	$26,%xmm8,%xmm10
	vpaddq	%xmm10,%xmm9,%xmm9
	vpand	.LandMask(%rip),%xmm8,%xmm8
	vpsrlq	$26,%xmm9,%xmm10
	vpsllq	$2,%xmm10,%xmm11
	vpaddq	%xmm11,%xmm10,%xmm10
	vpaddq	%xmm10,%xmm5,%xmm5
	vpand	.LandMask(%rip),%xmm9,%xmm9
	vpsrlq	$26,%xmm5,%xmm10
	vpand	.LandMask(%rip),%xmm5,%xmm5
	vpaddq	%xmm10,%xmm6,%xmm6
	vpsrlq	$26,%xmm6,%xmm10
	vpand	.LandMask(%rip),%xmm6,%xmm6
	vpaddq	%xmm10,%xmm7,%xmm7
	vpsrlq	$26,%xmm7,%xmm10
	vpand	.LandMask(%rip),%xmm7,%xmm7
	vpaddq	%xmm10,%xmm8,%xmm8
	vpsrlq	$26,%xmm8,%xmm10
	vpand	.LandMask(%rip),%xmm8,%xmm8
	vpaddq	%xmm10,%xmm9,%xmm9

	vmovdqu	%xmm5,64(%rdi)
	vmovdqu	%xmm6,96(%rdi)
	vmovdqu	%xmm7,128(%rdi)
	vmovdqu	%xmm8,160(%rdi)
	vmovdqu	%xmm9,192(%rdi)

	vpsllq	$2,%xmm6,%xmm1
	vpsllq	$2,%xmm7,%xmm2
	vpsllq	$2,%xmm8,%xmm3
	vpsllq	$2,%xmm9,%xmm4

	vpaddq	%xmm1,%xmm6,%xmm1
	vpaddq	%xmm2,%xmm7,%xmm2
	vpaddq	%xmm3,%xmm8,%xmm3
	vpaddq	%xmm4,%xmm9,%xmm4

	vmovdqu	%xmm1,224(%rdi)
	vmovdqu	%xmm2,256(%rdi)
	vmovdqu	%xmm3,288(%rdi)
	vmovdqu	%xmm4,320(%rdi)

	movq	$1,56(%rdi)

	.byte	0xf3,0xc3
.size	poly1305_init_avx2,.-poly1305_init_avx2



.globl	poly1305_update_avx2
.type	poly1305_update_avx2, @function
.align	64
poly1305_update_avx2:


	testq	%rdx,%rdx
	jz	6f

	cmpq	$0,56(%rdi)
	jne	1f



	cmpq	$512,%rdx
	jge	2f

	jmp	poly1305_update_x64

2:
	call	poly1305_init_avx2

1:
	vmovd	352(%rdi),%xmm0
	vmovd	356(%rdi),%xmm1
	vmovd	360(%rdi),%xmm2
	vmovd	364(%rdi),%xmm3
	vmovd	368(%rdi),%xmm4

	vmovdqa	.LandMask(%rip),%ymm12
1:
	cmpq	$128,%rdx
	jb	1f
	subq	$64,%rdx


	vmovdqu	0(%rsi),%ymm9
	vmovdqu	32(%rsi),%ymm10
	addq	$64,%rsi

	vpunpcklqdq	%ymm10,%ymm9,%ymm7
	vpunpckhqdq	%ymm10,%ymm9,%ymm8

	vpermq	$0xD8,%ymm7,%ymm7
	vpermq	$0xD8,%ymm8,%ymm8

	vpsrlq	$26,%ymm7,%ymm9
	vpand	%ymm12,%ymm7,%ymm7
	vpaddq	%ymm7,%ymm0,%ymm0

	vpsrlq	$26,%ymm9,%ymm7
	vpand	%ymm12,%ymm9,%ymm9
	vpaddq	%ymm9,%ymm1,%ymm1

	vpsllq	$12,%ymm8,%ymm9
	vpxor	%ymm9,%ymm7,%ymm7
	vpand	%ymm12,%ymm7,%ymm7
	vpaddq	%ymm7,%ymm2,%ymm2

	vpsrlq	$26,%ymm9,%ymm7
	vpsrlq	$40,%ymm8,%ymm9
	vpand	%ymm12,%ymm7,%ymm7
	vpxor	.LsetBit(%rip),%ymm9,%ymm9
	vpaddq	%ymm7,%ymm3,%ymm3
	vpaddq	%ymm9,%ymm4,%ymm4


	vpbroadcastq	64(%rdi),%ymm5
	vpmuludq	%ymm5,%ymm0,%ymm7
	vpmuludq	%ymm5,%ymm1,%ymm8
	vpmuludq	%ymm5,%ymm2,%ymm9
	vpmuludq	%ymm5,%ymm3,%ymm10
	vpmuludq	%ymm5,%ymm4,%ymm11

	vpbroadcastq	224(%rdi),%ymm5
	vpmuludq	%ymm5,%ymm4,%ymm6
	vpaddq	%ymm6,%ymm7,%ymm7
	vpbroadcastq	96(%rdi),%ymm5
	vpmuludq	%ymm5,%ymm0,%ymm6
	vpaddq	%ymm6,%ymm8,%ymm8
	vpmuludq	%ymm5,%ymm1,%ymm6
	vpaddq	%ymm6,%ymm9,%ymm9
	vpmuludq	%ymm5,%ymm2,%ymm6
	vpaddq	%ymm6,%ymm10,%ymm10
	vpmuludq	%ymm5,%ymm3,%ymm6
	vpaddq	%ymm6,%ymm11,%ymm11

	vpbroadcastq	256(%rdi),%ymm5
	vpmuludq	%ymm5,%ymm3,%ymm6
	vpaddq	%ymm6,%ymm7,%ymm7
	vpmuludq	%ymm5,%ymm4,%ymm6
	vpaddq	%ymm6,%ymm8,%ymm8
	vpbroadcastq	128(%rdi),%ymm5
	vpmuludq	%ymm5,%ymm0,%ymm6
	vpaddq	%ymm6,%ymm9,%ymm9
	vpmuludq	%ymm5,%ymm1,%ymm6
	vpaddq	%ymm6,%ymm10,%ymm10
	vpmuludq	%ymm5,%ymm2,%ymm6
	vpaddq	%ymm6,%ymm11,%ymm11

	vpbroadcastq	288(%rdi),%ymm5
	vpmuludq	%ymm5,%ymm2,%ymm6
	vpaddq	%ymm6,%ymm7,%ymm7
	vpmuludq	%ymm5,%ymm3,%ymm6
	vpaddq	%ymm6,%ymm8,%ymm8
	vpmuludq	%ymm5,%ymm4,%ymm6
	vpaddq	%ymm6,%ymm9,%ymm9
	vpbroadcastq	160(%rdi),%ymm5
	vpmuludq	%ymm5,%ymm0,%ymm6
	vpaddq	%ymm6,%ymm10,%ymm10
	vpmuludq	%ymm5,%ymm1,%ymm6
	vpaddq	%ymm6,%ymm11,%ymm11

	vpbroadcastq	320(%rdi),%ymm5
	vpmuludq	%ymm5,%ymm1,%ymm6
	vpaddq	%ymm6,%ymm7,%ymm7
	vpmuludq	%ymm5,%ymm2,%ymm6
	vpaddq	%ymm6,%ymm8,%ymm8
	vpmuludq	%ymm5,%ymm3,%ymm6
	vpaddq	%ymm6,%ymm9,%ymm9
	vpmuludq	%ymm5,%ymm4,%ymm6
	vpaddq	%ymm6,%ymm10,%ymm10
	vpbroadcastq	192(%rdi),%ymm5
	vpmuludq	%ymm5,%ymm0,%ymm6
	vpaddq	%ymm6,%ymm11,%ymm11

	vpsrlq	$26,%ymm10,%ymm5
	vpaddq	%ymm5,%ymm11,%ymm11
	vpand	%ymm12,%ymm10,%ymm10

	vpsrlq	$26,%ymm11,%ymm5
	vpsllq	$2,%ymm5,%ymm6
	vpaddq	%ymm6,%ymm5,%ymm5
	vpaddq	%ymm5,%ymm7,%ymm7
	vpand	%ymm12,%ymm11,%ymm11

	vpsrlq	$26,%ymm7,%ymm5
	vpand	%ymm12,%ymm7,%ymm0
	vpaddq	%ymm5,%ymm8,%ymm8
	vpsrlq	$26,%ymm8,%ymm5
	vpand	%ymm12,%ymm8,%ymm1
	vpaddq	%ymm5,%ymm9,%ymm9
	vpsrlq	$26,%ymm9,%ymm5
	vpand	%ymm12,%ymm9,%ymm2
	vpaddq	%ymm5,%ymm10,%ymm10
	vpsrlq	$26,%ymm10,%ymm5
	vpand	%ymm12,%ymm10,%ymm3
	vpaddq	%ymm5,%ymm11,%ymm4
	jmp	1b
1:

	cmpq	$64,%rdx
	jb	1f
	subq	$64,%rdx

	vmovdqu	0(%rsi),%ymm9
	vmovdqu	32(%rsi),%ymm10
	addq	$64,%rsi

	vpunpcklqdq	%ymm10,%ymm9,%ymm7
	vpunpckhqdq	%ymm10,%ymm9,%ymm8

	vpermq	$0xD8,%ymm7,%ymm7
	vpermq	$0xD8,%ymm8,%ymm8

	vpsrlq	$26,%ymm7,%ymm9
	vpand	%ymm12,%ymm7,%ymm7
	vpaddq	%ymm7,%ymm0,%ymm0

	vpsrlq	$26,%ymm9,%ymm7
	vpand	%ymm12,%ymm9,%ymm9
	vpaddq	%ymm9,%ymm1,%ymm1

	vpsllq	$12,%ymm8,%ymm9
	vpxor	%ymm9,%ymm7,%ymm7
	vpand	%ymm12,%ymm7,%ymm7
	vpaddq	%ymm7,%ymm2,%ymm2

	vpsrlq	$26,%ymm9,%ymm7
	vpsrlq	$40,%ymm8,%ymm9
	vpand	%ymm12,%ymm7,%ymm7
	vpxor	.LsetBit(%rip),%ymm9,%ymm9
	vpaddq	%ymm7,%ymm3,%ymm3
	vpaddq	%ymm9,%ymm4,%ymm4


	vmovdqu	64(%rdi),%ymm5
	vpmuludq	%ymm5,%ymm0,%ymm7
	vpmuludq	%ymm5,%ymm1,%ymm8
	vpmuludq	%ymm5,%ymm2,%ymm9
	vpmuludq	%ymm5,%ymm3,%ymm10
	vpmuludq	%ymm5,%ymm4,%ymm11

	vmovdqu	224(%rdi),%ymm5
	vpmuludq	%ymm5,%ymm4,%ymm6
	vpaddq	%ymm6,%ymm7,%ymm7
	vmovdqu	96(%rdi),%ymm5
	vpmuludq	%ymm5,%ymm0,%ymm6
	vpaddq	%ymm6,%ymm8,%ymm8
	vpmuludq	%ymm5,%ymm1,%ymm6
	vpaddq	%ymm6,%ymm9,%ymm9
	vpmuludq	%ymm5,%ymm2,%ymm6
	vpaddq	%ymm6,%ymm10,%ymm10
	vpmuludq	%ymm5,%ymm3,%ymm6
	vpaddq	%ymm6,%ymm11,%ymm11

	vmovdqu	256(%rdi),%ymm5
	vpmuludq	%ymm5,%ymm3,%ymm6
	vpaddq	%ymm6,%ymm7,%ymm7
	vpmuludq	%ymm5,%ymm4,%ymm6
	vpaddq	%ymm6,%ymm8,%ymm8
	vmovdqu	128(%rdi),%ymm5
	vpmuludq	%ymm5,%ymm0,%ymm6
	vpaddq	%ymm6,%ymm9,%ymm9
	vpmuludq	%ymm5,%ymm1,%ymm6
	vpaddq	%ymm6,%ymm10,%ymm10
	vpmuludq	%ymm5,%ymm2,%ymm6
	vpaddq	%ymm6,%ymm11,%ymm11

	vmovdqu	288(%rdi),%ymm5
	vpmuludq	%ymm5,%ymm2,%ymm6
	vpaddq	%ymm6,%ymm7,%ymm7
	vpmuludq	%ymm5,%ymm3,%ymm6
	vpaddq	%ymm6,%ymm8,%ymm8
	vpmuludq	%ymm5,%ymm4,%ymm6
	vpaddq	%ymm6,%ymm9,%ymm9
	vmovdqu	160(%rdi),%ymm5
	vpmuludq	%ymm5,%ymm0,%ymm6
	vpaddq	%ymm6,%ymm10,%ymm10
	vpmuludq	%ymm5,%ymm1,%ymm6
	vpaddq	%ymm6,%ymm11,%ymm11

	vmovdqu	320(%rdi),%ymm5
	vpmuludq	%ymm5,%ymm1,%ymm6
	vpaddq	%ymm6,%ymm7,%ymm7
	vpmuludq	%ymm5,%ymm2,%ymm6
	vpaddq	%ymm6,%ymm8,%ymm8
	vpmuludq	%ymm5,%ymm3,%ymm6
	vpaddq	%ymm6,%ymm9,%ymm9
	vpmuludq	%ymm5,%ymm4,%ymm6
	vpaddq	%ymm6,%ymm10,%ymm10
	vmovdqu	192(%rdi),%ymm5
	vpmuludq	%ymm5,%ymm0,%ymm6
	vpaddq	%ymm6,%ymm11,%ymm11

	vpsrlq	$26,%ymm10,%ymm5
	vpaddq	%ymm5,%ymm11,%ymm11
	vpand	%ymm12,%ymm10,%ymm10
	vpsrlq	$26,%ymm11,%ymm5
	vpsllq	$2,%ymm5,%ymm6
	vpaddq	%ymm6,%ymm5,%ymm5
	vpaddq	%ymm5,%ymm7,%ymm7
	vpand	%ymm12,%ymm11,%ymm11
	vpsrlq	$26,%ymm7,%ymm5
	vpand	%ymm12,%ymm7,%ymm0
	vpaddq	%ymm5,%ymm8,%ymm8
	vpsrlq	$26,%ymm8,%ymm5
	vpand	%ymm12,%ymm8,%ymm1
	vpaddq	%ymm5,%ymm9,%ymm9
	vpsrlq	$26,%ymm9,%ymm5
	vpand	%ymm12,%ymm9,%ymm2
	vpaddq	%ymm5,%ymm10,%ymm10
	vpsrlq	$26,%ymm10,%ymm5
	vpand	%ymm12,%ymm10,%ymm3
	vpaddq	%ymm5,%ymm11,%ymm4

	vpsrldq	$8,%ymm0,%ymm7
	vpsrldq	$8,%ymm1,%ymm8
	vpsrldq	$8,%ymm2,%ymm9
	vpsrldq	$8,%ymm3,%ymm10
	vpsrldq	$8,%ymm4,%ymm11

	vpaddq	%ymm7,%ymm0,%ymm0
	vpaddq	%ymm8,%ymm1,%ymm1
	vpaddq	%ymm9,%ymm2,%ymm2
	vpaddq	%ymm10,%ymm3,%ymm3
	vpaddq	%ymm11,%ymm4,%ymm4

	vpermq	$0xAA,%ymm0,%ymm7
	vpermq	$0xAA,%ymm1,%ymm8
	vpermq	$0xAA,%ymm2,%ymm9
	vpermq	$0xAA,%ymm3,%ymm10
	vpermq	$0xAA,%ymm4,%ymm11

	vpaddq	%ymm7,%ymm0,%ymm0
	vpaddq	%ymm8,%ymm1,%ymm1
	vpaddq	%ymm9,%ymm2,%ymm2
	vpaddq	%ymm10,%ymm3,%ymm3
	vpaddq	%ymm11,%ymm4,%ymm4
1:
	testq	%rdx,%rdx
	jz	5f

	vmovq	%xmm0,%xmm0
	vmovq	%xmm1,%xmm1
	vmovq	%xmm2,%xmm2
	vmovq	%xmm3,%xmm3
	vmovq	%xmm4,%xmm4

	movq	.LsetBit(%rip),%rcx
	movq	%rsp,%rax
	testq	$15,%rdx
	jz	1f
	xorq	%rcx,%rcx
	subq	$64,%rsp
	vpxor	%ymm7,%ymm7,%ymm7
	vmovdqu	%ymm7,(%rsp)
	vmovdqu	%ymm7,32(%rsp)
3:
	movb	(%rsi,%rcx), %r8b
	movb	%r8b,(%rsp,%rcx)
	incq	%rcx
	cmpq	%rcx,%rdx
	jne	3b

	movb	$1,(%rsp,%rcx)
	xorq	%rcx,%rcx
	movq	%rsp,%rsi

1:

	cmpq	$16,%rdx
	ja	2f
	vmovq	0(%rsi),%xmm7
	vmovq	8(%rsi),%xmm8
	vmovq	%rcx,%xmm14
	vmovdqa	.LpermFix(%rip),%ymm13
	jmp	1f
2:
	cmpq	$32,%rdx
	ja	2f
	vmovdqu	0(%rsi),%xmm9
	vmovdqu	16(%rsi),%xmm10
	vmovq	.LsetBit(%rip),%xmm14
	vpinsrq	$1,%rcx,%xmm14,%xmm14
	vmovdqa	.LpermFix+32(%rip),%ymm13

	vpunpcklqdq	%ymm10,%ymm9,%ymm7
	vpunpckhqdq	%ymm10,%ymm9,%ymm8
	jmp	1f
2:
	cmpq	$48,%rdx
	ja	2f
	vmovdqu	0(%rsi),%ymm9
	vmovdqu	32(%rsi),%xmm10
	vmovq	.LsetBit(%rip),%xmm14
	vpinsrq	$1,%rcx,%xmm14,%xmm14
	vpermq	$0xc4,%ymm14,%ymm14
	vmovdqa	.LpermFix+64(%rip),%ymm13

	vpunpcklqdq	%ymm10,%ymm9,%ymm7
	vpunpckhqdq	%ymm10,%ymm9,%ymm8
	jmp	1f
2:
	vmovdqu	0(%rsi),%ymm9
	vmovdqu	32(%rsi),%ymm10
	vmovq	.LsetBit(%rip),%xmm14
	vpinsrq	$1,%rcx,%xmm14,%xmm14
	vpermq	$0x40,%ymm14,%ymm14
	vmovdqa	.LpermFix+96(%rip),%ymm13

	vpunpcklqdq	%ymm10,%ymm9,%ymm7
	vpunpckhqdq	%ymm10,%ymm9,%ymm8

1:
	movq	%rax,%rsp

	vpsrlq	$26,%ymm7,%ymm9
	vpand	%ymm12,%ymm7,%ymm7
	vpaddq	%ymm7,%ymm0,%ymm0

	vpsrlq	$26,%ymm9,%ymm7
	vpand	%ymm12,%ymm9,%ymm9
	vpaddq	%ymm9,%ymm1,%ymm1

	vpsllq	$12,%ymm8,%ymm9
	vpxor	%ymm9,%ymm7,%ymm7
	vpand	%ymm12,%ymm7,%ymm7
	vpaddq	%ymm7,%ymm2,%ymm2

	vpsrlq	$26,%ymm9,%ymm7
	vpsrlq	$40,%ymm8,%ymm9
	vpand	%ymm12,%ymm7,%ymm7
	vpxor	%ymm14,%ymm9,%ymm9
	vpaddq	%ymm7,%ymm3,%ymm3
	vpaddq	%ymm9,%ymm4,%ymm4


	vmovdqu	64(%rdi),%ymm5
	vpermd	%ymm5,%ymm13,%ymm5
	vpmuludq	%ymm5,%ymm0,%ymm7
	vpmuludq	%ymm5,%ymm1,%ymm8
	vpmuludq	%ymm5,%ymm2,%ymm9
	vpmuludq	%ymm5,%ymm3,%ymm10
	vpmuludq	%ymm5,%ymm4,%ymm11

	vmovdqu	224(%rdi),%ymm5
	vpermd	%ymm5,%ymm13,%ymm5
	vpmuludq	%ymm5,%ymm4,%ymm6
	vpaddq	%ymm6,%ymm7,%ymm7
	vmovdqu	96(%rdi),%ymm5
	vpermd	%ymm5,%ymm13,%ymm5
	vpmuludq	%ymm5,%ymm0,%ymm6
	vpaddq	%ymm6,%ymm8,%ymm8
	vpmuludq	%ymm5,%ymm1,%ymm6
	vpaddq	%ymm6,%ymm9,%ymm9
	vpmuludq	%ymm5,%ymm2,%ymm6
	vpaddq	%ymm6,%ymm10,%ymm10
	vpmuludq	%ymm5,%ymm3,%ymm6
	vpaddq	%ymm6,%ymm11,%ymm11

	vmovdqu	256(%rdi),%ymm5
	vpermd	%ymm5,%ymm13,%ymm5
	vpmuludq	%ymm5,%ymm3,%ymm6
	vpaddq	%ymm6,%ymm7,%ymm7
	vpmuludq	%ymm5,%ymm4,%ymm6
	vpaddq	%ymm6,%ymm8,%ymm8
	vmovdqu	128(%rdi),%ymm5
	vpermd	%ymm5,%ymm13,%ymm5
	vpmuludq	%ymm5,%ymm0,%ymm6
	vpaddq	%ymm6,%ymm9,%ymm9
	vpmuludq	%ymm5,%ymm1,%ymm6
	vpaddq	%ymm6,%ymm10,%ymm10
	vpmuludq	%ymm5,%ymm2,%ymm6
	vpaddq	%ymm6,%ymm11,%ymm11

	vmovdqu	288(%rdi),%ymm5
	vpermd	%ymm5,%ymm13,%ymm5
	vpmuludq	%ymm5,%ymm2,%ymm6
	vpaddq	%ymm6,%ymm7,%ymm7
	vpmuludq	%ymm5,%ymm3,%ymm6
	vpaddq	%ymm6,%ymm8,%ymm8
	vpmuludq	%ymm5,%ymm4,%ymm6
	vpaddq	%ymm6,%ymm9,%ymm9
	vmovdqu	160(%rdi),%ymm5
	vpermd	%ymm5,%ymm13,%ymm5
	vpmuludq	%ymm5,%ymm0,%ymm6
	vpaddq	%ymm6,%ymm10,%ymm10
	vpmuludq	%ymm5,%ymm1,%ymm6
	vpaddq	%ymm6,%ymm11,%ymm11

	vmovdqu	320(%rdi),%ymm5
	vpermd	%ymm5,%ymm13,%ymm5
	vpmuludq	%ymm5,%ymm1,%ymm6
	vpaddq	%ymm6,%ymm7,%ymm7
	vpmuludq	%ymm5,%ymm2,%ymm6
	vpaddq	%ymm6,%ymm8,%ymm8
	vpmuludq	%ymm5,%ymm3,%ymm6
	vpaddq	%ymm6,%ymm9,%ymm9
	vpmuludq	%ymm5,%ymm4,%ymm6
	vpaddq	%ymm6,%ymm10,%ymm10
	vmovdqu	192(%rdi),%ymm5
	vpermd	%ymm5,%ymm13,%ymm5
	vpmuludq	%ymm5,%ymm0,%ymm6
	vpaddq	%ymm6,%ymm11,%ymm11

	vpsrlq	$26,%ymm10,%ymm5
	vpaddq	%ymm5,%ymm11,%ymm11
	vpand	%ymm12,%ymm10,%ymm10
	vpsrlq	$26,%ymm11,%ymm5
	vpsllq	$2,%ymm5,%ymm6
	vpaddq	%ymm6,%ymm5,%ymm5
	vpaddq	%ymm5,%ymm7,%ymm7
	vpand	%ymm12,%ymm11,%ymm11
	vpsrlq	$26,%ymm7,%ymm5
	vpand	%ymm12,%ymm7,%ymm0
	vpaddq	%ymm5,%ymm8,%ymm8
	vpsrlq	$26,%ymm8,%ymm5
	vpand	%ymm12,%ymm8,%ymm1
	vpaddq	%ymm5,%ymm9,%ymm9
	vpsrlq	$26,%ymm9,%ymm5
	vpand	%ymm12,%ymm9,%ymm2
	vpaddq	%ymm5,%ymm10,%ymm10
	vpsrlq	$26,%ymm10,%ymm5
	vpand	%ymm12,%ymm10,%ymm3
	vpaddq	%ymm5,%ymm11,%ymm4

	vpsrldq	$8,%ymm0,%ymm7
	vpsrldq	$8,%ymm1,%ymm8
	vpsrldq	$8,%ymm2,%ymm9
	vpsrldq	$8,%ymm3,%ymm10
	vpsrldq	$8,%ymm4,%ymm11

	vpaddq	%ymm7,%ymm0,%ymm0
	vpaddq	%ymm8,%ymm1,%ymm1
	vpaddq	%ymm9,%ymm2,%ymm2
	vpaddq	%ymm10,%ymm3,%ymm3
	vpaddq	%ymm11,%ymm4,%ymm4

	vpermq	$0xAA,%ymm0,%ymm7
	vpermq	$0xAA,%ymm1,%ymm8
	vpermq	$0xAA,%ymm2,%ymm9
	vpermq	$0xAA,%ymm3,%ymm10
	vpermq	$0xAA,%ymm4,%ymm11

	vpaddq	%ymm7,%ymm0,%ymm0
	vpaddq	%ymm8,%ymm1,%ymm1
	vpaddq	%ymm9,%ymm2,%ymm2
	vpaddq	%ymm10,%ymm3,%ymm3
	vpaddq	%ymm11,%ymm4,%ymm4

5:
	vmovd	%xmm0,352(%rdi)
	vmovd	%xmm1,356(%rdi)
	vmovd	%xmm2,360(%rdi)
	vmovd	%xmm3,364(%rdi)
	vmovd	%xmm4,368(%rdi)
6:
	.byte	0xf3,0xc3
.size	poly1305_update_avx2,.-poly1305_update_avx2


.type	poly1305_finish_avx2,@function
.globl	poly1305_finish_avx2
poly1305_finish_avx2:
	cmpq	$0,56(%rdi)
	jne	1f
	jmp	poly1305_finish_x64

1:
	movl	352(%rdi),%r8d
	movl	356(%rdi),%eax
	movl	360(%rdi),%r9d
	movl	364(%rdi),%ecx
	movl	368(%rdi),%r10d

	shlq	$26,%rax
	addq	%rax,%r8

	movq	%r9,%rax
	shlq	$52,%rax
	shrq	$12,%r9
	addq	%rax,%r8
	adcq	$0,%r9

	movq	%r10,%rax
	shlq	$14,%rcx
	shlq	$40,%rax
	shrq	$24,%r10

	addq	%rcx,%r9
	addq	%rax,%r9
	adcq	$0,%r10

	movq	%r10,%rax
	shrq	$2,%rax
	andq	$3,%r10

	movq	%rax,%rcx
	shlq	$2,%rax
	addq	%rcx,%rax

	addq	%rax,%r8
	adcq	$0,%r9
	adcq	$0,%r10

	movq	%r8,%rax
	movq	%r9,%rcx
	subq	$-5,%rax
	sbbq	$-1,%rcx
	sbbq	$3,%r10

	cmovcq	%r8,%rax
	cmovcq	%r9,%rcx
	addq	40(%rdi),%rax
	adcq	40+8(%rdi),%rcx
	movq	%rax,(%rsi)
	movq	%rcx,8(%rsi)

	.byte	0xf3,0xc3
.size	poly1305_finish_avx2,.-poly1305_finish_avx2
