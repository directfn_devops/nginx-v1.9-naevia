
.LrSet:
.align	16
.quad	0x0FFFFFFC0FFFFFFF, 0x0FFFFFFC0FFFFFFC



.globl	poly1305_init_x64
.type	poly1305_init_x64, @function
.align	64
poly1305_init_x64:

	xorq	%rax,%rax
	movq	%rax,0(%rdi)
	movq	%rax,8(%rdi)
	movq	%rax,16(%rdi)

	movdqu	0(%rsi),%xmm0
	movdqu	16(%rsi),%xmm1
	pand	.LrSet(%rip),%xmm0

	movdqu	%xmm0,24(%rdi)
	movdqu	%xmm1,24+16(%rdi)
	movq	$0,56(%rdi)

	.byte	0xf3,0xc3
.size	poly1305_init_x64,.-poly1305_init_x64


.globl	poly1305_update_x64
.type	poly1305_update_x64, @function
.align	64
poly1305_update_x64:

	pushq	%r11
	pushq	%r12
	pushq	%r13
	pushq	%r14
	pushq	%r15

	movq	%rdx,%r10

	movq	0(%rdi),%rcx
	movq	8(%rdi),%r8
	movq	16(%rdi),%r9
	movq	24(%rdi),%r15

	cmpq	$16,%r10
	jb	2f
	jmp	1f

.align	64
1:

	addq	0(%rsi),%rcx
	adcq	8(%rsi),%r8
	leaq	16(%rsi),%rsi
	adcq	$1,%r9

5:
	movq	%r15,%rax
	mulq	%rcx
	movq	%rax,%r11
	movq	%rdx,%r12

	movq	%r15,%rax
	mulq	%r8
	addq	%rax,%r12
	adcq	$0,%rdx

	movq	%r15,%r13
	imul	%r9,%r13
	addq	%rdx,%r13

	movq	32(%rdi),%rax
	mulq	%rcx
	addq	%rax,%r12
	adcq	$0,%rdx
	movq	%rdx,%rcx

	movq	32(%rdi),%rax
	mulq	%r8
	addq	%rcx,%r13
	adcq	$0,%rdx
	addq	%rax,%r13
	adcq	$0,%rdx

	movq	32(%rdi),%r14
	imul	%r9,%r14
	addq	%rdx,%r14


	movq	%r11,%rcx
	movq	%r12,%r8
	movq	%r13,%r9
	andq	$3,%r9

	movq	%r13,%r11
	movq	%r14,%r12

	andq	$-4,%r11
	shrdq	$2,%r14,%r13
	shrq	$2,%r14

	addq	%r11,%rcx
	adcq	%r12,%r8
	adcq	$0,%r9

	addq	%r13,%rcx
	adcq	%r14,%r8
	adcq	$0,%r9

	subq	$16,%r10
	cmpq	$16,%r10
	jae	1b

2:
	testq	%r10,%r10
	jz	3f

	movq	$1,%r11
	xorq	%r12,%r12
	xorq	%r13,%r13
	addq	%r10,%rsi

4:
	shldq	$8,%r11,%r12
	shlq	$8,%r11
	movzbq	-1(%rsi),%r13
	xorq	%r13,%r11
	decq	%rsi
	decq	%r10
	jnz	4b

	addq	%r11,%rcx
	adcq	%r12,%r8
	adcq	$0,%r9

	movq	$16,%r10
	jmp	5b

3:

	movq	%rcx,0(%rdi)
	movq	%r8,8(%rdi)
	movq	%r9,16(%rdi)

	popq	%r15
	popq	%r14
	popq	%r13
	popq	%r12
	popq	%r11
	.byte	0xf3,0xc3
.size	poly1305_update_x64, .-poly1305_update_x64


.type	poly1305_finish_x64,@function
.align	64
.globl	poly1305_finish_x64
poly1305_finish_x64:

	movq	0(%rdi),%rcx
	movq	8(%rdi),%rax
	movq	16(%rdi),%rdx

	movq	%rcx,%r8
	movq	%rax,%r9
	movq	%rdx,%r10

	subq	$-5,%rcx
	sbbq	$-1,%rax
	sbbq	$3,%rdx

	cmovcq	%r8,%rcx
	cmovcq	%r9,%rax
	cmovcq	%r10,%rdx

	addq	40(%rdi),%rcx
	adcq	48(%rdi),%rax
	movq	%rcx,(%rsi)
	movq	%rax,8(%rsi)

	.byte	0xf3,0xc3
.size	poly1305_finish_x64, .-poly1305_finish_x64
