# Nginx-1.9.9 (Naevia) Web Service for Mubasher|DirectFN #

This is how we compiled Nginx with new security and performance features. Please refer nginx.spec file for compiled options.

Here build Nginx with ChaCha20 and Poly1305 protocols on OpenSSL for better performances for Mobile devices.
```
$ cd rpmbuild/SOURCE
$ wget https://www.openssl.org/source/old/1.0.2/openssl-1.0.2g.tar.gz
$ tar -zxvf openssl-1.0.2g.tar.gz
$ cd openssl-1.0.2g
$ patch -p1 < openssl__chacha20_poly1305_draft_and_rfc_ossl102g.patch
$ ./config reconfig
```
Note that CHACHA20 patch doesn't work with GCC 4.4 hence we have to use devtoolset-3 GCC to compile. and this is how we install devtoolset-3 on CentOS v6

```
$ cd /etc/yum.repos.d
$ wget https://copr.fedorainfracloud.org/coprs/rhscl/devtoolset-3/repo/epel-6/rhscl-devtoolset-3-epel-6.repo
$ yum install devtoolset-3-gcc.x86_64 devtoolset-3-gcc-c++.x86_64 -y
```
Make sure Nginx build process will use devtoolset-gcc 4.9 instead of 4.4 with the CC variable
```
$ export CC=/opt/rh/devtoolset-3/root/usr/bin/cc
```

Now let's build the Nginx package
```
$ cd SPECS
$ rpmbuild -bb nginx-1.9.9
...
/home/hirantha/rpmbuild/RPMS/x86_64/nginx-1.9.9-4.el6.mfs.x86_64.rpm
```